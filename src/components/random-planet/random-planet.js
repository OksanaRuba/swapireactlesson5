import React, { useContext, useState, useEffect } from 'react';
import {NameContext} from '../app/app';

import './random-planet.css';

const RandomPlanet = () => {
  const[information, setInformation] = useState([])
  
  console.warn('information', information);
  
  const {profileInformation} = useContext(NameContext);
  console.warn('profileInformation', profileInformation)
  
  useEffect(() => {
    setInformation(profileInformation)
  },[profileInformation]) 
  

    return (
      <div className="random-planet jumbotron rounded">
        <img className="planet-image"
             src="https://starwars-visualguide.com/assets/img/planets/5.jpg" />
        <div>
          <h4>Planet Name</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Population</span>
              <span>{information.population}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Rotation Period</span>
              <span>{information.rotation_period}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Diameter</span>
              <span>{information.diameter}</span>
            </li>
          </ul>
        </div>
      </div>

    );
  }

export default RandomPlanet;