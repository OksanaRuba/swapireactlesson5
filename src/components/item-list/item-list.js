import React, { useState, useEffect, useContext } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Service from '../../services/service';
import { NameContext } from '../app/app';
import './item-list.css';

const ItemList = ({ request }) => {
  const [property, setProperty] = useState(null)
  useEffect(() => {
    request.then((data) => {
      setProperty(data.results);
    })
    /* setProperty() //зберігаємо нові дані */
  }, []);

  const { onElementInfo } = useContext(NameContext);

  return (
    <ul className="item-list list-group">
      {property === null ? <CircularProgress /> : property.map((item, index) => {
        return <li
          className="list-group-item"
          key={index * 3 + 'r'}
          onClick={() => onElementInfo(item)}> {item.name} </li>
      })}
    </ul>
  );

}

export default ItemList;
